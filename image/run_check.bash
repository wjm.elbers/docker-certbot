#!/usr/bin/env bash

#
# Print certificate content:
#   > openssl x509 -in fullchain.pem -text -noout
#
# TODO:
# - manage crl in private ca mode
#
#

MODE_SELF_SIGNED="SELF_SIGNED"
MODE_CA_SIGNED="CA_SIGNED"
MODE_LETSENCRYPT="LETSENCRYPT"

cert_file_name="fullchain.pem"
key_file_name="privkey.pem"

#
# Log a message prefixed with a timestamp and the supplied ${stage}
#
# Parameters:
#   $1  stage
#   $2  msg
#
log() {
  local stage=$1
  local level=$2
  local msg=$3
  local date
  date=$(date +"%Y-%m-%d %T")

  printf "[%s] [%9s] [%5s] %s\n" "${date}" "${stage}" "${level}" "${msg}"
}

#
# Log a message prefixed with a timestamp and the supplied ${stage} and log all the content of the supplied file
#
# Parameters:
#   $1  stage
#   $2  msg
#
log_file() {
  local stage=$1
  local level=$2
  local msg=$3
  local file=$4

  log "${stage}" "${level}" "${msg}"
  while read -r line; do
    log "$stage" "${level}" "${line}"
  done < "${file}"

  if [ "${level}" == "fatal" ]; then
    exit 1
  fi
}

#
# Generate a self signed certificate with no password
#
# Parameters:
#   $1    stage
#   $2    domain
#   $3    cert_dir
#   $4    days
#
create_self_signed() {
  local stage=$1
  local domain=$2
  local cert_dir=$3
  local days=$4

  log "${stage}" "info" "Generating a self signed certificate..."

  if ! openssl req \
        -newkey rsa:4096 \
        -days "${days}" \
        -nodes \
        -x509 \
        -subj "/CN=${domain}" \
        -reqexts SAN \
        -extensions SAN \
        -config <(cat /etc/ssl/openssl.cnf \
          <(printf '[SAN]\nsubjectAltName=DNS:%s' "${domain}")) \
        -keyout "${cert_dir}/${key_file_name}" \
        -out "${cert_dir}/${cert_file_name}" > /var/log/cert.log 2>&1; then
    log_file "$stage" "fatal" "Failed to generate self signed certificate" "/var/log/cert.log"
  fi
}

#
# Generate a new CA certificate and key file
#
create_ca() {
  local stage=$1
  log "${stage}" "Generating new CA"
  if ! openssl req \
        -newkey rsa:4096 \
        -days "1825" \
        -nodes \
        -x509 \
        -subj "/CN=homeCA" \
        -keyout "/ca/ca.key" \
        -out "/ca/ca.pem" > /var/log/ca.log 2>&1; then
    log_file "$stage" "fatal" "Failed to generate CA" "/var/log/ca.log"
  fi
}

#
# Generate a CA signed certifacate and key file
#
# References:
# - https://gist.github.com/fntlnz/cf14feb5a46b2eda428e000157447309
# - https://stackoverflow.com/a/53826340
create_ca_signed() {
  local stage=$1
  local domain=$2
  local cert_dir=$3
  local days=$4

  log "${stage}" "info" "Generating a CA signed certificate..."

  if [ ! -f "/ca/ca.pem" ]; then
    log "${stage}" "info" "CA (/ca/ca.pem) not found"
    create_ca "${stage}"
  else
    log "${stage}" "info" "Using existing CA (/ca/ca.pem)"
  fi

  cd /tmp || exit 1

  #Generate private key and remove password
  if ! openssl genrsa -des3 -passout 'pass:xxxxx' -out 'server.pass.key' 4096  > /var/log/cert.log 2>&1; then
    log_file "$stage" "fatal" "Failed to generate key file" "/var/log/cert.log"
  fi
  if ! openssl rsa -passin 'pass:xxxxx' -in 'server.pass.key' -out "${key_file_name}"  >> /var/log/cert.log 2>&1; then
    log_file "$stage" "fatal" "Failed to remove passphrase from key file" "/var/log/cert.log"
  fi
  rm -f "server.pass.key"

  #Generate config file to get v3 support
  cp /etc/ssl/openssl.cnf /tmp/openssl.cnf
  printf '[SAN]\nsubjectAltName=DNS:%s' "${domain}" >> /tmp/openssl.cnf

  #Generate CSR
  if ! openssl req \
        -new \
        -nodes \
        -extensions v3_req \
        -reqexts SAN \
        -config /tmp/openssl.cnf \
        -subj "/C=NL/ST=Gelderland/L=Nijmegen/O=CLARIN ERIC/OU=IT Department/CN=${domain}" \
        -key "${key_file_name}" \
        -out "${cert_file_name}.csr" >> /var/log/cert.log 2>&1; then
    log_file "$stage" "fatal" "Failed to create certificate request" "/var/log/cert.log"
  fi

  #Sign certificate from CSR
  if ! openssl x509 \
        -req \
        -in "${cert_file_name}.csr" \
        -CA "/ca/ca.pem" \
        -CAkey "/ca/ca.key" \
        -CAcreateserial \
        -out "${cert_file_name}" \
        -days "${days}" \
        -sha256 >> /var/log/cert.log 2>&1; then
    log_file "$stage" "fatal" "Failed to sign certificate" "/var/log/cert.log"
  fi

  #Cleanup
  mv "${cert_file_name}" "${cert_dir}/${cert_file_name}"
  mv "${key_file_name}" "${cert_dir}/${key_file_name}"
  rm -f "openssl.cnf"
}

create_letsencrypt() {
  local stage=$1
  log "${stage}" "info" "Generating a letsencrypt signed certificate..."

  # Convert space separated list fof domains into argument array for certbot
  # Example:
  #   "localhost www.test.com" --> -d localhost -d www.test.com
  # shellcheck disable=SC2206
  local domains_array=(${DOMAINS})
  local le_domains=("${domains_array[*]/#/-d }")

  local staging=""
  # shellcheck disable=SC2153
  if [ "${LE_STAGING}" == "1" ]; then
    staging=" --test-cert"
    log "${stage}" "info" "Running certbot in staging mode"
  fi

  # shellcheck disable=SC2128,SC2086
  if ! certbot certonly \
      --non-interactive \
      --agree-tos \
      --renew-by-default \
      --email "${LE_EMAIL}" \
      --webroot \
      -w "/webroot" \
      ${staging} ${le_domains} > /var/log/certbot.log 2>&1; then
    log_file "$stage" "fatal" "Failed to get letsencrypt certificate(s)" "/var/log/certbot.log"
  fi

  cp "/etc/letsencrypt/live/${domains_array[0]}/fullchain.pem" "/shared/${cert_file_name}"
  cp "/etc/letsencrypt/live/${domains_array[0]}/privkey.pem" "/shared/${key_file_name}"
}

wait_for_nginx() {
  local stage=$1
  local nginx=$2

  log "${stage}" "info" "Waiting for nginx (https://${nginx}) to start"
  # shellcheck disable=SC2259
  while ! echo exit | curl -k "https://${nginx}" > /dev/null 2>&1 ; do
    log "${stage}" "info" "Waiting for nginx (https://${nginx}) to start"; sleep "1";
  done
  log "${stage}" "info" "Received valid nginx response"
}

# Check if certificate files exist, if not create an initial certifiate so that nginx can start
bootstrap() {
  local domain=$1
  local cert_dir=$2
  local nginx=$3

  log "bootstrap" "info" "Bootstap start."
  if [[ ! -e $cert_file ]]; then
    log "bootstrap" "info" "no certificate found."
    create_self_signed "bootstrap" "${domain}" "${cert_dir}" 5
    if [ "${nginx}" != "" ]; then
      wait_for_nginx "bootstrap" "${nginx}"
    else
      log "bootstrap" "info" "'NGINX_HOSTNAME' not set, not waiting for nginx"
    fi
  fi
  log "bootstrap" "info" "Bootstrap done"
}

# Check if the certificate exceeded or is close to its expiration date. If so, generate a new certificate
check_expiration() {
  local domain=$1
  local cert_dir=$2
  local mode=$3
  local cert_file="${cert_dir}/${cert_file_name}"
  local exp_limit=15

  log "renew" "info" "Renewal start"

  local exp
  exp=$(date -d "$(openssl x509 -in "$cert_file" -text -noout|grep "Not After"|cut -c 25-)" +%s)
  local datenow
  datenow=$(date -d "now" +%s)
  local days_exp=$(( ( exp - datenow ) / 86400 ))

  log "renew" "info" "Checking expiration date for ${domain}. Days exp_limit=${exp_limit} days..."

  if [ "$days_exp" -gt "$exp_limit" ] ; then
    log "renew" "info" "The certificate is up to date, no need for renewal ($days_exp days left)."
  else
    log "renew" "info" "The certificate for ${domain} is about to expire in ${days_exp} day(s). Starting renewal script..."
    if [ "${mode}" == "${MODE_SELF_SIGNED}" ]; then
      create_self_signed "renew" "${domain}" "${cert_dir}" 90
    elif [ "${mode}" == "${MODE_CA_SIGNED}" ]; then
      create_ca_signed "renew" "${domain}" "${cert_dir}" 90
    elif [ "${mode}" == "${MODE_LETSENCRYPT}" ]; then
      create_letsencrypt  "renew" "${domain}" "${cert_dir}"
    else
      log "renew" "info" "Unsupported MODE=${mode}. Supported modes: ${MODE_SELF_SIGNED}, ${MODE_CA_SIGNED}, ${MODE_LETSENCRYPT}"
    fi
    log "renew" "info" "Renewal process finished for domain ${domain}"
  fi

  log "renew" "info" "Renewal done"
}

if [ -x ${CERT_DIR_PATH+x} ]; then
    log "init" "fatal" "CERT_DIR_PATH is required"
    exit 1
fi
if [ -x ${DOMAINS+x} ]; then
  log "init" "fatal" "DOMAINS is required"
  exit 1
fi

_MODE="${MODE_SELF_SIGNED}"
if [ ! -x ${MODE+x} ]; then
  _MODE="${MODE}"
fi

if [ "${_MODE}" == "${MODE_LETSENCRYPT}" ]; then
  if [ -x ${LE_EMAIL+x} ]; then
    log "init" "fatal" "LE_EMAIL is required in letsencrypt mode"
    exit 1
  fi
fi

log "init" "info" "Mode=${_MODE}"

bootstrap "${DOMAINS}" "${CERT_DIR_PATH}" "${NGINX_HOSTNAME}"
check_expiration "${DOMAINS}" "${CERT_DIR_PATH}" "${_MODE}"