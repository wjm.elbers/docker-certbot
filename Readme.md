## Functionality
* Generate self signed certificate to bootstrap and allow nginx to start
* Generate a CA signed certificate
* Generate initial letsencrypt certificate(s)
* Renew when close to expiration date

## Configuration

Required:
* DOMAINS="<your domains>"
* EMAIL="<your contact email>"

Optional:
* CERT_DIR_PATH="/certbot/letsencrypt"
* WEBROOT_PATH="/certbot/www"
* EXP_LIMIT="30"
* CHECK_FREQ="30"
* CHICKENEGG="1"
* STAGING="0"

* LE_RENEW_HOOK="docker restart nginx"; # <--- change to your nginx server docker container name

## References

Based / inspired on:
* [docker-crontab](https://github.com/willfarrell/docker-crontab)  by Will Farell
* Docker certbot [gist](https://gist.github.com/maxivak/4706c87698d14e9de0918b6ea2a41015) by maxivak

Extending:
* https://hub.docker.com/r/certbot/certbot

Letsencrypt:
* [Rate Limits](https://letsencrypt.org/docs/rate-limits/)